
### Suave Pickel Seven

A toy to practice using websockets and so on.

Run by starting server, then navigating a web browser to index.html

Required libraries : [Java-WebSocket](https://search.maven.org/#artifactdetails%7Corg.java-websocket%7CJava-WebSocket%7C1.3.0%7Cjar) and [json](https://search.maven.org/#artifactdetails%7Corg.json%7Cjson%7C20160807%7Cbundle)

#### Run

Interaction involves putting text in the box, choosing a mangling algorithm, and pressing the button. The picture will change depending upon the algorithm chosen

#### Reason

I intend to make localhost client-server programs and hadn't used websockets before. I'd rather start with a doodle than the project I want to actually port.

'Suave Pickele Seven' is just a three word phrase chosen to avoid titling this project 'websocket demo #25392'. It has no other overt significance.

