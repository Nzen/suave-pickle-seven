
/*
*/

var termin = function() {};
termin.log = function( message )
{
	try
	{ console.log( message ); }
	catch ( exception )
	{ return; } // IE reputedly has no console.
}


var supported = "WebSocket" in window;
if ( ! supported )
{
	document.getElementById( "btnSubmit" ).disabled = true; 
}


// @deprecated
function picCorrespondsToRadio( whetherReverse )
{
	if ( whetherReverse )
	{
		return "now.png";
	}
	else
	{
		return "later.png";
	}
}


function getInput()
{
	var tfName = document.getElementById( "tfName" ).value;
	var whetherReverse = document.getElementById( "rev" ).checked;
	return { "reqType" : "transform",
		"tfName" : tfName, "isReverse" : whetherReverse };
}


function applyResponse( stillJson )
{
	var response = JSON.parse( stillJson );
	if ( response.ansType == "show" )
	{
	document.getElementById( "tfOut" ).textContent = response.tfName;
	document.getElementById( "thePic" ).src = picCorrespondsToRadio( response.isReverse );
	}
	else
	{
		termin.log( response.ansType +" is not a response I handle" );
		// perhaps other failure states
	}
}


// not callable from button unless browser supports websockets; caller's responsibility otherwise
function changeName()
{
	var channel = new WebSocket( "ws://localhost:9998" );
	channel.onopen = function coo()
	{
		channel.send( JSON.stringify(getInput()) );
	};
	channel.onclose = function coc()
	{
		termin.log( "glad that's over" );
	};
	channel.onmessage = function com( msg )
	{
		applyResponse( msg.data );
		channel.close();
	};
}


function linkAsCallingConvention()
{
	// http://stackoverflow.com/questions/7755088/what-does-href-expression-a-href-javascript-a-do
	termin.log( "mind cracked" );
}


/*
someone suggested recreating a textElement for changing text of a div
	http://stackoverflow.com/a/121898

http://www.w3schools.com/jsref/dom_obj_event.asp
	onclick onmouseleave onkeypress

https://www.tutorialspoint.com/html5/html5_websocket.htm
	websocket skeleton

*/



























